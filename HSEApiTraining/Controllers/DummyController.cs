﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace HSEApiTraining.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DummyController : Controller
    {
        private readonly Random rand;

        private readonly Services.IDummyService _dummyService;

        public DummyController(Services.IDummyService dummyService)
        {
            rand = new Random();
            _dummyService = dummyService;
        }

        [HttpGet("generate/{number}")]
        public string DummyGenerator(int number)
        {
            if (number < 0 ){
                return "invalid number";
            }
            return Convert.ToString(_dummyService.GetRandomNum(number));
        }
    }
}
