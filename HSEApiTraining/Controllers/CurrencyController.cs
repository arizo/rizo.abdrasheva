﻿using Microsoft.AspNetCore.Mvc;
using HSEApiTraining.Models.Currency;
using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using HSEApiTraining.Services;

namespace HSEApiTraining.Controllers
{
    //Тут все методы-хендлеры вам нужно реализовать самим
    [Route("api/currency")]
    [ApiController]
    public class CurrencyController : Controller
    {
        private readonly Services.ICurrencyService _currencyService;
        public CurrencyController(Services.ICurrencyService currencyService)
        {
            _currencyService = currencyService;
        }

        [HttpGet]
        public List<string> CurrencyGet([FromQuery] string Symbol, string DateS, string DateE)
        {
            CurrencyRequest cRequest = _currencyService.BuildRequest(Symbol, DateS, DateE);
            List<string> currencys = _currencyService.GetCurrencys(cRequest);
            if (Symbol == null)
            {
                currencys.Add("INVALID REQUEST");
                return currencys;
            }
            if (currencys.Count != 0)
            {
                return currencys;
            }
            else
            {
                currencys.Add("INVALID REQUEST");
                return currencys;
            }
        }
        //[HttpPost]
        //public void CurrencyPost([FromBody] CurrencyRequest cRequest)
        //{
            
        //}
        
    }
}
