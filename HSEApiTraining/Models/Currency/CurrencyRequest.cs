﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HSEApiTraining.Models.Currency
{
    public class CurrencyRequest
    {
        DateTime dateStart;
        DateTime dateEnd;
        public string Symbol { get; set; }
        public DateTime DateStart
        {
            get
            {
                return dateStart;
            }
            set
            {
                dateStart = value;
            }
        }
        public DateTime DateEnd
        {
            get
            {
                return dateEnd;
            }
            set
            {
                dateEnd = value;
            }
        }
    }
}
