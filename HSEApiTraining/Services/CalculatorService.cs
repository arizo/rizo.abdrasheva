﻿using System;
using System.Collections.Generic;

namespace HSEApiTraining
{
    public interface ICalculatorService
    {
        double CalculateExpression(string expression);
        IEnumerable<double> CalculateBatchExpressions(IEnumerable<string> expressions);
    }

    public class CalculatorService : ICalculatorService
    {
        public IEnumerable<double> CalculateBatchExpressions(IEnumerable<string> expressions)
        {
            throw new NotImplementedException();
        }

        public double CalculateExpression(string expr)
        {
            string expr1 = expr.Replace('.', ',');
            for (int i = 0; i < expr1.Length; i++)
            {
                  if (expr1[i] != '0' && expr1[i] != '1' && expr1[i] != '2' && expr1[i] != '3' && expr1[i] != '4' 
                   && expr1[i] != '5' && expr1[i] != '6' && expr1[i] != '7' && expr1[i] != '8' &&
                   expr1[i] != '9' && expr1[i] != '.' && expr1[i] != ',' && expr1[i] != '+' &&
                   expr1[i] != '-' && expr1[i] != '*' && expr1[i] != '/' && expr1[i] != '%' && expr1[i] != ' ') throw new Exception("wrong");
            }
            double multi = 0;
            if (Convert.ToString(expr1[0]) == "*" || Convert.ToString(expr1[expr1.Length - 1]) == "*" || Convert.ToString(expr1[0]) == "+"
            || Convert.ToString(expr1[0]) == "/" || Convert.ToString(expr1[0]) == "%"
            || Convert.ToString(expr1[expr1.Length - 1]) == "+"
            || Convert.ToString(expr1[expr1.Length - 1]) == "-"
            || Convert.ToString(expr1[expr1.Length - 1]) == "/"
            || Convert.ToString(expr1[expr1.Length - 1]) == "%") throw new ArgumentException("Not correct format of input string!");
            for (int i = 1; i < expr1.Length - 1; i++)
            {
                if (Convert.ToString(expr1[i]) == "*")
                {
                    if (Convert.ToString(expr1[i + 1]) == "+") throw new ArgumentException("Error: *+");
                    multi = double.Parse(expr1.Substring(0, i)) * double.Parse(expr1.Substring(i + 1, expr1.Length - 1 - i));
                    break;
                }
                else
                if (Convert.ToString(expr1[i]) == "/")
                {
                    if (Convert.ToString(expr1[i + 1]) == "+") throw new ArgumentException("Error: /+");
                    if (double.Parse(expr1.Substring(i + 1, expr1.Length - 1 - i)) == 0) throw new ArgumentException("Error: divide by zero!!!");
                    multi = double.Parse(expr1.Substring(0, i)) / double.Parse(expr1.Substring(i + 1, expr1.Length - 1 - i));
                    break;
                }
                else
                if (Convert.ToString(expr1[i]) == "%")
                {
                    if (Convert.ToString(expr1[i + 1]) == "+") throw new ArgumentException("Error: %+");
                    multi = double.Parse(expr1.Substring(0, i)) % double.Parse(expr1.Substring(i + 1, expr1.Length - 1 - i));
                    break;
                }
                else
                if (Convert.ToString(expr1[i]) == "+")
                {
                    if (Convert.ToString(expr1[i + 1]) == "+") throw new ArgumentException("Error: ++");
                    else
                        multi = double.Parse(expr1.Substring(0, i)) + double.Parse(expr1.Substring(i + 1, expr1.Length - 1 - i));
                    break;
                }
                else
                if (Convert.ToString(expr1[i]) == "-")
                {
                    if (Convert.ToString(expr1[i + 1]) == "+") throw new ArgumentException("Error: -+");
                    multi = double.Parse(expr1.Substring(0, i)) - double.Parse(expr1.Substring(i + 1, expr1.Length - 1 - i));
                    break;
                }
            }
            if (multi > int.MaxValue || multi < int.MinValue) throw new ArgumentException("Value");
            return Math.Round(multi, 3);
            
        }
    }
}
