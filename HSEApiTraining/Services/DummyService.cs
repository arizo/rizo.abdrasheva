﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HSEApiTraining.Services
{
    public interface IDummyService
    {
        int GetRandomNum(int i);
    }

    public class DummyService : IDummyService
    {
        public Random rand = new Random();
        public int GetRandomNum(int i)
        {
            return rand.Next(i);
        }
    }
}
