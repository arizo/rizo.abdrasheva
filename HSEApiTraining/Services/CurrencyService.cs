﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HSEApiTraining.Models.Currency;


namespace HSEApiTraining.Services
{
    public interface ICurrencyService
    {
        string GetHtmlPage(string dt);
        void CurrencyMethod();
        string Rate(string text, string currency);
        CurrencyRequest BuildRequest(string Symbol, string DateS, string DateE);
        List<string> GetCurrencys(CurrencyRequest cRequest);
    }

    public class CurrencyService : ICurrencyService
    {
        public List<string> GetCurrencys(CurrencyRequest cRequest)
        {
            DateTime end = cRequest.DateEnd;
            DateTime start = cRequest.DateStart;
            string cur = cRequest.Symbol;
            List<DateTime> dateTimes = new List<DateTime>();
            for (var day = start.Date; day.Date <= end.Date; day = day.AddDays(1))
            {
                dateTimes.Add(day);
                Console.WriteLine(day);
            }
            string pattern = "yyyy-MM-dd";
            List<string> newDateTimes = new List<string>();
            List<string> currencys = new List<string>();
            foreach (var dates in dateTimes)
            {
                string test = string.Format("{0:yyyy-MM-dd}", dates); // .toshortdate
                newDateTimes.Add(test);
            }
            for (int i = 0; i < dateTimes.Count; i++)
            {
                string s;
                try
                {
                    s = this.GetHtmlPage(newDateTimes[i]);
                }
                catch
                {
                    break;
                }
                currencys.Add(this.Rate(s, cur));
            }
            return currencys;
        }
        public CurrencyRequest BuildRequest(string Symbol, string DateS, string DateE)
        {
            DateTime DS;
            DateTime DE;

            if (DateS == null || !DateTime.TryParse(DateS, out DateTime d1))
            {
                DS = DateTime.Now;
            }
            else
            {
                DS = DateTime.Parse(DateS);
            }

            if (DateE == null || !DateTime.TryParse(DateE, out DateTime d2))
            {
                DE = DateTime.Now;
            }
            else
            {
                DE = DateTime.Parse(DateE);
            }
            CurrencyRequest cRequest = new CurrencyRequest
            {
                DateEnd = DE,
                DateStart = DS,
                Symbol = Symbol
            };
            return cRequest;
        }
        public string GetHtmlPage(string dt)
        {
            string url = "https://api.ratesapi.io/api/" + dt + "?base=RUB";
            string HtmlText = string.Empty;
            HttpWebRequest myHttwebrequest = (HttpWebRequest)HttpWebRequest.Create(url);
            HttpWebResponse myHttpWebresponse = (HttpWebResponse)myHttwebrequest.GetResponse();
            StreamReader strm = new StreamReader(myHttpWebresponse.GetResponseStream());
            HtmlText = strm.ReadToEnd();
            return HtmlText;
        }
        public string Rate(string text, string currency)
        {
            string outppp = "";
            string output = "";
            string[] arr = text.Split('"');
            for (int i = 0; i < arr.Length - 1; i++)
            {
                if (arr[i] == currency)
                {
                    output = arr[i + 1];
                }
            }
            for (int i = 0; i < output.Length; i++)
            {
                // забыла поменять точки на запятые
                // exception, оценка 3
                if (output[i] != ':' && output[i] != ',' && output[i] != '}') outppp += output[i];
            }

            return outppp;
        }
        public void CurrencyMethod()
        {
            return;
        }
    }
}
